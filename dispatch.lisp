;;;; dispatch.lisp
;;;; this file contains the dispatch table setup
(in-package #:playwright)


(defun playwright-populate-dispatch-table ()
  (push
   (hunchentoot:create-prefix-dispatcher "/version" 'playwright-version)
   (dispatch-table *playwright-server*)))


(defun playwright-version ()
  (let* ((response `((useragent .
                                ,(hunchentoot:user-agent
                                  hunchentoot:*request*))
                     (version . ,*playwright-version*)))
         (json-response (json:encode-json-to-string response)))
    (setf (hunchentoot:content-type*) "application/json")
    (format nil "~A" json-response)))

(defun playwright-post ())
