#!/usr/bin/env python

import requests
import simplejson

SERVER = "https://localhost:8443/version"

def test(server):
        print '[+] requesting version'
        r = requests.get(server, verify=False)
        try:
                version = simplejson.loads(r.content)
        except Exception as exc:
                print '[!] exception: ' + str(exc)
                print '\tstatus code: %d' % (r.status_code, )
                print '\tbody: %s' % (r.content, )
                return
        if not r.status_code == 200:
                print '[!] error loading page'
                return
        else:
                print '[+] successfully retrieved version'
                for key in version:
                        print '\t%s: %s' % (key, version[key])
                return

if __name__ == '__main__':
        test(SERVER)



