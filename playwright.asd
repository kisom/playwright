;;;; playwright.asd

(asdf:defsystem #:playwright
  :serial t
  :description "playwright is an HTTPS-based NLP service."
  :author "Kyle Isom <coder@kyleisom.net>"
  :license "ISC license"
  :depends-on (#:hunchentoot
               #:postmodern
               #:cl-redis
               #:cl-json
               #:ironclad
               #:drakma)
  :components ((:file "package")
               (:file "playwright")
               (:file "dispatch")))
