;;;; playwright.lisp
;;;; this file contains the server set up and initialisation code
(in-package #:playwright)

;;; "playwright" goes here. Hacks and glory await!
(defparameter *server-address* "127.0.0.1")
(defparameter *server-port* 8001)
(defparameter *server-ssl-key* "ssl-keys/test.key")
(defparameter *server-ssl-cert* "ssl-keys/test.cert")
(defparameter *playwright-version* "0.1.0")
(defparameter *playwright-https-log* "logs/hunchentoot.log")
(defparameter *playwright-access-log* "logs/access.log")
(defparameter *playwright-document-root* "static/")
(defparameter *linebreak* (concatenate 'string (string #\return)
                                       (string #\newline)))
(defvar *playwright-server*)
(defvar *request*)

(defmacro make-server-class (name acceptor)
  `(defclass ,name (,acceptor)
    ((dispatch-table
      :initform '()
      :accessor dispatch-table
      :documentation "List of dispatch functions"))
    (:default-initargs
     :address *server-address*)))

(defmacro add-dispatch-method (server-class)
  `(defmethod hunchentoot:acceptor-dispatch-request
       ((,server-class ,server-class) request)
     (mapc (lambda (dispatcher)
             (let ((handler (funcall dispatcher request)))
               (when handler
                 (return-from hunchentoot:acceptor-dispatch-request
                   (funcall handler)))))
           (dispatch-table ,server-class))
(call-next-method)))

(defun build-classes ()
    (progn
      ;;; create SSL server class
      (make-server-class secure-server hunchentoot:ssl-acceptor)
      (add-dispatch-method secure-server)

      ;;; create HTTP server class
      (make-server-class insecure-server hunchentoot:acceptor)
      (add-dispatch-method insecure-server)))

(defun init-server (&key (server-port *server-port*) (secure nil))
  (if secure
      (make-instance 'secure-server
                     :port server-port
                     :ssl-certificate-file *server-ssl-cert*
                     :ssl-privatekey-file *server-ssl-key*)
      (make-instance 'insecure-server
                     :port server-port)))

(defun start (&key (server-port *server-port*) (secure nil))
  (progn
    (build-classes) ; recompile classes as necessary
    (setf *playwright-server* (init-server :server-port server-port
                                           :secure secure))
    (if secure
        (format t "server key: ~A~%server certificate: ~A~%" *server-ssl-key*
                *server-ssl-cert*))
    (setf (hunchentoot:acceptor-document-root *playwright-server*)
          *playwright-document-root*)
    (setf (hunchentoot:acceptor-message-log-destination *playwright-server*)
          *playwright-https-log*)
    (setf (hunchentoot:acceptor-access-log-destination *playwright-server*)
          *playwright-access-log*)
    (playwright-populate-dispatch-table)
    (if secure
        (format t "[+] will serve TLS~%")
        (format t "[+] will serve HTTP~%"))
    (format t "[+] will listen on ~A:~A~%" *server-address* *server-port*)
    (hunchentoot:start *playwright-server*)))

(defun stop ()
  (hunchentoot:stop *playwright-server*))

(defun reload (&key (server-port *server-port*) (secure nil))
  (stop)
  (ql:quickload :playwright)
  (start :server-port server-port :secure secure))

(defun set-key-dir (keydir)
  (setf *server-ssl-key* (merge-pathnames keydir *server-ssl-key*))
  (setf *server-ssl-key* (merge-pathnames keydir *server-ssl-key*)))

(defun set-address (address)
  (defparameter *server-address* address))
